import cv2
import numpy as np

# Create a simple image with nested rectangles
image = np.zeros((400, 400), dtype=np.uint8)
cv2.rectangle(image, (50, 50), (350, 350), 255, -1)
cv2.rectangle(image, (100, 100), (300, 300), 0, -1)
cv2.rectangle(image, (150, 150), (250, 250), 255, -1)

# Find contours with hierarchy
contours, hierarchy = cv2.findContours(image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

# Print contours and hierarchy
print("Contours:")
for i, contour in enumerate(contours):
    print(f"Contour {i}: {contour}")

print("\nHierarchy:")
print(hierarchy)

# Draw contours and hierarchy for visualization
output_image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)

for i, contour in enumerate(contours):
    color = (0, 255, 0)  # Green for contours
    cv2.drawContours(output_image, contours, i, color, 2)
    cv2.putText(output_image, f"{i}", tuple(contour[0][0]), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 1)

# Show the output image with contours labeled
cv2.imshow("Contours", output_image)
cv2.waitKey(0)
cv2.destroyAllWindows()
