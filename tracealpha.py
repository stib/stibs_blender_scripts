import bpy
import subprocess
import sys
import numpy as np

from bpy_extras.io_utils import ImportHelper
from bpy.types import Operator
from bpy.props import StringProperty


def trace_alpha(img_src, expansion_pixels, alpha_threshold, tolerance):
    # traces the alpha channel on an image to return closed loops of points that surround each area of non-transparent alpha
    import cv2
    import numpy as np
    # Load the image with alpha channel
    image = cv2.imread(img_src, cv2.IMREAD_UNCHANGED)
    # Ensure the image has an alpha channel
    if image.shape[2] == 4:
        # Extract the alpha channel
        alpha_channel = image[:, :, 3]

        # Create a kernel for dilation
        kernel = np.ones((expansion_pixels, expansion_pixels), np.uint8)

        # Dilate the mask
        dilated_alpha = cv2.dilate(alpha_channel, kernel, iterations=1)

        # Threshold the alpha channel to get a binary image
        _, binary_image = cv2.threshold(dilated_alpha, alpha_threshold, 255, cv2.THRESH_BINARY) #dilated_alpha

        # Find contours from the binary image
        contours, hierarchy = cv2.findContours(binary_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        contours = simplify_data(contours, float)
        hierarchy = simplify_data(hierarchy, int)
        contours = scale_contours(contours, image.shape)
        # contours is a tuple containing tuples of points that define loops 
        # (
        #   (
        #       (x00, y00), (x01, y01) … (x0n, y0n)
        #   ),
        #   … 
        #   (
        #       (xn0, yn0), (xn1, yn1), … (xnn, ynn)
        #   )
        # )
        # hierarchy contains 4 element tuples for each element of contours
        # (
        #   (n0, pr0, c0, p0)
        #   …
        #   (nn, prn, cn, pn)
        # )
        # n = next sibling
        # pr = previous sibling
        # c = first child
        # p = parent

    else:
        print("Image does not have an alpha channel")
        # output a rectangle the size of the image
        w = image.shape[0]
        h = image.shape[1]
        contours = (((0, 0), (w, 0), (w, h), (0, h),),)
        hierarchy = ((-1, -1, -1, -1),)
        contours = scale_contours(contours, image.shape)
    return{"contours": contours, "hierarchy": hierarchy}

def scale_contours(contours, image_size, target_size = (1.0, 1.0)):
    # Calculate the scale factor for each dimension
    scale_factor = [target_size[i] / image_size[i] for i in range(2)]
    # Apply the smallest scale factor to maintain proportions
    min_scale_factor = min(scale_factor)
    print("Scale factor: " + str(min_scale_factor))
    # Apply the transformations
    # probably the most pythonic bit of code I've ever written
    scaled_contours = tuple(tuple(tuple(dim * min_scale_factor for dim in point) for point in contour) for contour in contours)
    return scaled_contours


# DE-arrayify the openCV points,
def simplify_data(arr, type=float):
    # annoyingly the opencv_points are arrays of arrays of single member arrays containing a 2 member point
    # e.g
    # [[[[123,45]], [[12, 34]], … ], … ]
    # This outputs a tuple of tuples containing points
    # (((123,45), (12, 34), … ), … )
    if not isinstance(arr, (list, np.ndarray, tuple)):
        return type(arr)
    if len(arr) == 1:
        return simplify_data(arr[0], type)
    return tuple(simplify_data(item, type) for item in arr)

def outer_contours(image_data):
    contours = image_data["contours"]
    hierarchy = image_data["hierarchy"]
    parents = []
    
    outPolys = []
    print(hierarchy)
    for index, element in enumerate(hierarchy):
        # element[3] is the parent field
        if element[3] == -1: # no parent
            print(":l 56: index ", index) #Dbg
            parents.append(index)
         
    return [contours[i] for i in parents]

# Function to convert OpenCV points to a Blender polygon
def create_blender_polygon(opencv_points, scale):
    # Create a new mesh
    mesh = bpy.data.meshes.new(name="ImagePolygon")

    # Create a new object with the mesh
    obj = bpy.data.objects.new(name="ImagePolygon", object_data=mesh)

    # Link the object to the current collection
    bpy.context.collection.objects.link(obj)
    vertices = []
    even = False
    # Create vertices from points (adding a Z-coordinate of 0)
    for x, y in opencv_points:
        vertices.append((x, y, 0))


    # Create a single face from the vertices
    edges = []
    faces = [list(range(len(vertices)))]

    # Assign vertices, edges, and faces to the mesh
    mesh.from_pydata(vertices, edges, faces)

    # Update mesh with new data
    mesh.update()
    return obj

# Function to install required modules
def install_modules():
    python_executable = sys.executable

    try:
        import cv2
        import numpy
        print("Modules already installed.")
    except ImportError:
        print("Installing required modules...")
        subprocess.check_call([python_executable, '-m', 'ensurepip'])
        subprocess.check_call([python_executable, '-m', 'pip', 'install', 'opencv-python'])
        subprocess.check_call([python_executable, '-m', 'pip', 'install', 'numpy'])

def create_material(image):
    material = bpy.data.materials.new(name="ImageMaterial")
    material.use_nodes = True
    nodes = material.node_tree.nodes
    links = material.node_tree.links
    nodes.clear()
    
    image_node = nodes.new(type='ShaderNodeTexImage')
    image_node.image = image
    bsdf_node = nodes.new(type='ShaderNodeBsdfDiffuse')
    output = nodes.new(type='ShaderNodeOutputMaterial')
    links.new(image_node.outputs['Color'], bsdf_node.inputs['Color'])
    links.new(bsdf_node.outputs['BSDF'], output.inputs['Surface'])
    return material

def apply_material(obj, material):
    if obj:
        if obj.data.materials:
            obj.data.materials[0] = material
        else:
            obj.data.materials.append(material)
    else:
        print("No active object selected.")

# choose the file
# class SelectImageOperator(bpy.types.Operator, ImportHelper):
#     bl_idname = "image.select_image"
#     bl_label = "Select Image"
#     filename_ext = "*.png;*.jpg;*.jpeg;*.bmp;*.tga;*.tiff;*.exr"
#
#     filter_glob: StringProperty(
#         default="*.png;*.jpg;*.jpeg;*.bmp;*.tga;*.tiff;*.exr",
#         options={'HIDDEN'}
#     )
#
#     def execute(self, context):
#         print( "Selected file: " + self.filepath)
#         return self.filepath
#
#
# the main hoo-hah
class Import_Image_With_Alpha(bpy.types.Operator):
    """Import an image with an alpha channel and trace the outline"""
    bl_idname = "object.import_and_trace_alpha"
    bl_label = "Import and trace alpha"
    bl_options = {'REGISTER', 'UNDO'}

    filepath: bpy.props.StringProperty(subtype="FILE_PATH")

    expansion_pixels: bpy.props.IntProperty(
        name = "expansion_pixels",
        description = "expand border (px)",
        default = 12
    )
    alpha_threshold: bpy.props.FloatProperty(
        name = "alpha_threshold",
        description = "Alpha threshold",
        default = 1
    )
    epsilon: bpy.props.FloatProperty(
        name = "epsilon",
        description = "accuracy",
        default = 0.001
    )
    
    # @classmethod
    #def poll(cls, context):
    #    return context.object is not None

    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        print(self.filepath)
        return {'RUNNING_MODAL'}

    def execute(self, context):
        # self.filepath =  SelectImageOperator('INVOKE_DEFAULT')
        image_data = trace_alpha(self.filepath, self.expansion_pixels, self.alpha_threshold, self.epsilon)
        outlines = outer_contours(image_data)

        image = bpy.data.images.load(self.filepath)
        material = create_material(image)
        for outline in outlines:
            outerPoly = create_blender_polygon(outline, 1.0)
            apply_material(outerPoly, material)
        return {"FINISHED"}


def menu_func(self, context):
    self.layout.operator(Import_Image_With_Alpha.bl_idname)

def register():
    install_modules()
    bpy.utils.register_class(Import_Image_With_Alpha)
    # bpy.utils.register_class(SelectImageOperator)
    bpy.types.TOPBAR_MT_file_import.append(menu_func)

def unregister():
    bpy.utils.unregister_class(Import_Image_With_Alpha)
    bpy.types.TOPBAR_MT_file_import.remove(menu_func)

if __name__ == "__main__":
    register()

