# stibs Blender Scripts
Some little tools for Blender. Just starting out with the Blender API, so expect bugs

## Save-n-zip
Zips old copies of the file yopu're working on when you save, so that versions don't litter your hard drive.

## Mirror KFs
Copies selected keyframes and pastes them mirrored around the current frame.
