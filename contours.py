import bpy
import subprocess
import sys
import numpy as np

def trace_alpha(img_src, expansion_pixels, alpha_threshold, tolerance):
    # traces the alpha channel on an image to return closed loops of points that surround each area of non-transparent alpha
    import cv2
    import numpy as np
    # Load the image with alpha channel
    image = cv2.imread(img_src, cv2.IMREAD_UNCHANGED)
    # Ensure the image has an alpha channel
    if image.shape[2] == 4:
        # Extract the alpha channel
        alpha_channel = image[:, :, 3]

        # Create a kernel for dilation
        kernel = np.ones((expansion_pixels, expansion_pixels), np.uint8)

        # Dilate the mask
        dilated_alpha = cv2.dilate(alpha_channel, kernel, iterations=1)

        # Threshold the alpha channel to get a binary image
        _, binary_image = cv2.threshold(dilated_alpha, alpha_threshold, 255, cv2.THRESH_BINARY) #dilated_alpha

        # Find contours from the binary image
        contours, hierarchy = cv2.findContours(binary_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        contours = simplify_data(contours, float)
        hierarchy = simplify_data(hierarchy, int)
        return {"contours": contours, "hierarchy": hierarchy}
    else:
        print("Image does not have an alpha channel")

# DE-arrayify the openCV points,
def simplify_data(arr, type=float):
    # annoyingly the opencv_points are arrays of arrays of single member arrays containing a 2 member point
    # e.g
    # [[[[123,45]], [[12, 34]], … ], … ]
    # This outputs a tuple of tuples containing points
    # (((123,45), (12, 34), … ), … )
    if not isinstance(arr, (list, np.ndarray, tuple)):
        return type(arr)
    if len(arr) == 1:
        return simplify_data(arr[0], type)
    return tuple(simplify_data(item, type) for item in arr)

def create_nested_loops(image_data):
    contours = image_data["contours"]
    hierarchy = image_data["hierarchy"]
    parents = []
    
    outPolys = []
    for index, element in enumerate(hierarchy):
        # element[3] is the parent field
        if element[3] == -1: # no parent
            print(":l 56: index ", index) #Dbg
            parents.append(index)
            contours.append(Contour(contours[index]))
    
    for index, element in enumerate(hierarchy):
        if element[3] in parents:
            contour[element[3]]
    return out_contour

def dist(p1, p2):
    import math
    return math.sqrt(math.pow(p1[0] - p2[0], 2) + math.pow(p1[1] - p2[1], 2))

class Poly:
    # a closed loop of non-repeating points
    def __init__(self, points):
        self.points = []
        for pnt in points:
            if points[pnt] not in self.points:
                self.points.append(points[pnt])
    

class Contour:
    def __init__(self, points):
        self.points = points
        self.polys = Poly(points)
        
    def merge_with_child(child):
        


# takes a hierarchical tree of nested loops and outputs a flat list of closed loops, each one a valid shape for a blender polygon
# contours is a tuple containing tuples of points that define loops 
# (
#   (
#       (x00, y00), (x01, y01) … (x0n, y0n)
#   ),
#   … 
#   (
#       (xn0, yn0), (xn1, yn1), … (xnn, ynn)
#   )
# )
# hierarchy contains 4 element tuples for each element of contours
# (
#   (n0, pr0, c0, p0)
#   …
#   (nn, prn, cn, pn)
# )
# n = next
# pr = previous
# c = first child
# p = parent



# function to add interior holes to exterior shapes
def join_loops(outer, inner):
    print("outer: " + str(outer))
    print("inner " + str(inner))

# Function to convert OpenCV points to a Blender polygon
def create_blender_polygon(opencv_points, scale):
    # Create a new mesh
    mesh = bpy.data.meshes.new(name="ImagePolygon")

    # Create a new object with the mesh
    obj = bpy.data.objects.new(name="ImagePolygon", object_data=mesh)

    # Link the object to the current collection
    bpy.context.collection.objects.link(obj)
    vertices = []
    even = False
    # Create vertices from points (adding a Z-coordinate of 0)
    for x, y in opencv_points:
        vertices.append((x, y, 0))


    # Create a single face from the vertices
    edges = []
    faces = [list(range(len(vertices)))]

    # Assign vertices, edges, and faces to the mesh
    mesh.from_pydata(vertices, edges, faces)

    # Update mesh with new data
    mesh.update()
    return obj

# Function to install required modules
def install_modules():
    python_executable = sys.executable

    try:
        import cv2
        import numpy
        print("Modules already installed.")
    except ImportError:
        print("Installing required modules...")
        subprocess.check_call([python_executable, '-m', 'ensurepip'])
        subprocess.check_call([python_executable, '-m', 'pip', 'install', 'opencv-python'])
        subprocess.check_call([python_executable, '-m', 'pip', 'install', 'numpy'])

# Example function that uses OpenCV
def process_image(image_path):
    img_src = "/home/sdixon/Desktop/test/alphatest.png"
    expansion_pixels = 12
    alpha_threshold = 1
    epsilon = 0.001
    image_data = trace_alpha(img_src, expansion_pixels, alpha_threshold, epsilon)
    outlines = create_nested_loops(image_data)
    for outline in outlines:
        outerPoly = create_blender_polygon(outline, 1.0)

# Example Blender operator that uses the function
class SimpleOperator(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "object.simple_operator"
    bl_label = "Simple Object Operator"

    def execute(self, context):
        process_image('/path/to/image.png')
        return {'FINISHED'}

def menu_func(self, context):
    self.layout.operator(SimpleOperator.bl_idname)

def register():
    install_modules()
    bpy.utils.register_class(SimpleOperator)
    bpy.types.VIEW3D_MT_object.append(menu_func)

def unregister():
    bpy.utils.unregister_class(SimpleOperator)
    bpy.types.VIEW3D_MT_object.remove(menu_func)

if __name__ == "__main__":
    register()

