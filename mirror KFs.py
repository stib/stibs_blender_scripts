import bpy

# Get the active action and the current frame
current_frame = bpy.context.scene.frame_current


# Loop through the selected f-curves in the action
action = bpy.data.actions[bpy.context.object.animation_data.action.name]
for fcurve in action.fcurves:
    # Check if the f-curve has selected keyframes
    
    if fcurve.keyframe_points:# Create a dictionary to store the copied keyframes
        copied_keyframes = {}
        # Loop through the selected keyframes of the f-curve
        for keyframe in fcurve.keyframe_points:
            if keyframe.select_control_point:
                # Store the keyframe in the dictionary with its value
                kfTime = keyframe.co[0]
                # and the time offset from the current frame
                copied_keyframes[kfTime - current_frame] = {
                    "value": keyframe.co[1], 
                    "type": keyframe.type,
                    'amplitude': keyframe.amplitude,
                    'back': keyframe.back,
                    'bl_rna': keyframe.bl_rna,
                    'co': keyframe.co,
                    'co_ui': keyframe.co_ui,
                    'easing': keyframe.easing,
                    'handle_left': [ keyframe.handle_right[0]  - kfTime, keyframe.handle_right[1]],
                    'handle_left_type': keyframe.handle_right_type,
                    'handle_right': [keyframe.handle_left[0] - kfTime, keyframe.handle_left[1]],
                    'handle_right_type': keyframe.handle_left_type,
                    'interpolation': keyframe.interpolation,
                    'period': keyframe.period,
                    'rna_type': keyframe.rna_type,
                    'select_control_point': keyframe.select_control_point,
                    'select_left_handle': keyframe.select_right_handle,
                    'select_right_handle': keyframe.select_left_handle,
                }

        # Loop through the copied keyframes and add them to the action in reverse
        for time_offset, details in copied_keyframes.items():
            # Calculate the frame number of the keyframe
            frame = current_frame - time_offset
            # Set the value of the keyframe at the calculated frame
            newKF = fcurve.keyframe_points.insert(
                frame, 
                details["value"], 
                options = {'FAST'}, 
                keyframe_type = details["type"]
                )
            # not sure how much of this info is strictly neccessary, but whatevs
            newKF.amplitude = details["amplitude"]
            newKF.back = details["back"]
            newKF.easing = details["easing"]
            newKF.handle_left = [ frame - details["handle_left"][0], details["handle_left"][1]]
            newKF.handle_left_type = details["handle_left_type"]
            newKF.handle_right = [ frame -  details["handle_right"][0], details["handle_right"][1]]
            newKF.handle_right_type = details["handle_right_type"]
            newKF.interpolation = details["interpolation"]
            newKF.select_control_point = details["select_control_point"]
            newKF.select_left_handle = details["select_left_handle"]
            newKF.select_right_handle = details["select_right_handle"]

