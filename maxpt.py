# List of tuples
tuples_list = [(1, 5), (2, 11), (4, 7), (9, 0), (8, 6)]

# Find the maximum value in the list of tuples
max_value = max(max(tup) for tup in tuples_list)

print("The maximum value in the list of tuples is:", max_value)