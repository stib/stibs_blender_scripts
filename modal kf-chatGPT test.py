import bpy

class MoveKeyframesModalOperator(bpy.types.Operator):
    """Move selected keyframes in the graph editor as the mouse moves"""
    bl_idname = "graph.move_keyframes_modal"
    bl_label = "Move Keyframes Modal"

    _timer = None
    _start_mouse_position = None

    @classmethod
    def poll(cls, context):
        return context.area.type == 'GRAPH_EDITOR'

    def modal(self, context, event):
        if event.type == 'MOUSEMOVE':
            # get the difference between the starting mouse position and the current mouse position
            delta = event.mouse_x - self._start_mouse_position[0]

            # move the selected keyframes by the delta
            for fcurve in context.active_object.animation_data.action.fcurves:
                for keyframe in fcurve.keyframe_points:
                    if keyframe.select_control_point:
                        mirror(keyframe)

            # update the starting mouse position
            self._start_mouse_position = event.mouse_x, event.mouse_y

            # redraw the viewport
            context.area.tag_redraw()

        elif event.type == 'LEFTMOUSE':
            # stop the modal operator
            return self.cancel(context)

        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        # start the modal operator
        context.window_manager.modal_handler_add(self)
        self._start_mouse_position = event.mouse_x, event.mouse_y
        return {'RUNNING_MODAL'}

    def cancel(self, context):
        # stop the modal operator
        context.window_manager.modal_handler_remove(self)
        return {'CANCELLED'}

def menu_func(self, context):
    self.layout.operator(MoveKeyframesModalOperator.bl_idname, text=MoveKeyframesModalOperator.bl_label)


# Register and add to the "view" menu (required to also use F3 search "Simple Modal Operator" for quick access).
def register():
    bpy.utils.register_class(MoveKeyframesModalOperator)
    bpy.types.GRAPH_MT_key.append(menu_func)

def unregister():
    bpy.utils.unregister_class(MoveKeyframesModalOperator)

if __name__ == "__main__":
    register()
