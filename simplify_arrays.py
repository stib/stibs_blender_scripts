def nested_to_tuple(arr):
    if not isinstance(arr, list):
        return arr
    if len(arr) == 1:
        return nested_to_tuple(arr[0])
    return tuple(nested_to_tuple(item) for item in arr)

# Example usage
input_array = [[[[0, 1]], [[0, 3]], [[123, 45]]]]
output_tuple = nested_to_tuple(input_array)
print(output_tuple)  # Output: ((0, 1), (0, 3), (123, 45))
