bl_info = {
    "name": "Rest On Floor",
    "author": "Flobo02",
    "version": (1, 0),
    "blender": (2, 80, 0),
    "location": "View3D > Object > Snap",
    "description": "Move object to floor",
    "warning": "",
    "wiki_url": "",
    "category": "Object",
    }
    
    import bpy
    
    #funcion
    def main(context):
        for obj in context.selected_objects:
            mx = obj.matrix_world
            minz = min((mx @ v.co)[2] for v in obj.data.vertices)
            mx.translation.z -= minz
    #operador
    class ATERRIZAR(bpy.types.Operator):
    
        bl_idname = "snap.aterrizar"
        bl_label = "Aterrizar"
    
        @classmethod
        def poll(cls, context):
            return context.active_object is not None
    
        def execute(self, context):
            main(context)
            return {'FINISHED'}
    #boton
    def boton(self, context):
        self.layout.operator( 
            ATERRIZAR.bl_idname,
            text="To floor",
            icon='TRIA_DOWN_BAR')
    
    # ubicacion
    bpy.types.VIEW3D_MT_snap.append(boton)
    
    def register():
        bpy.utils.register_class(ATERRIZAR)
    
    def unregister():
        bpy.utils.unregister_class(ATERRIZAR)
    
    if __name__ == "__main__":
        register()/* Add your custom extension styling here */
